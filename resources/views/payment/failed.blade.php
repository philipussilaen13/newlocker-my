<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<title></title>

		<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">		
		<link href="{{URL::asset('css/bootstrap-theme.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('css/payment.css')}}" rel="stylesheet">
		
		<script src="{{URL::asset('js/jquery.min.js')}}"></script>
		<script src="{{URL::asset('js/bootstrap.js')}}"></script>
	</head>
	<body>
		<div id="head">			
		</div>
		<div id="content">
			<div class="container container-fluid">
				<div class="row">					
					<div class="col-md-12 col-sm-12 col-xs-12 failed-img">
					<center>
						<img src="{{URL::asset('img/payment/failed.png')}}" width="96" height="96"><br/>
						<div class="word-failed">Payment Failed</div>
						<br>
						<div class="word-desc">We are sorry something went wrong<br/>
							Please try again
					</center>
						</div>
					</div>
				</div>
			</div>
<!-- failed<br/>
<?php print_r($response); ?> -->
		</div>
		<div id="footer"></div>
	</body>
</html>
