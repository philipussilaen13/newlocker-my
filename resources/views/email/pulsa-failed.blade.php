@if (!empty($sepulsaDb))
	<?php
		$sepulsaDb = (object)$sepulsaDb; 
	?>
	<p>Ada Transaksi Gagal {{ $sepulsaDb->product_type }} : <strong>{{ $sepulsaDb->invoice_id }}</strong></p>
	<table border="1">
		<tr>
			<td>Customer Data</td>
			<td>
				<strong>Phone</strong> : {{ $sepulsaDb->cust_phone }} <br>
				<strong>Email</strong>	: {{ $sepulsaDb->cust_email }}			
			</td>
		</tr>
		<tr>
			<td>Product Data</td>
			<td>
				<strong>Type</strong> : {{ $sepulsaDb->product_type }} <br>
				<strong>Product Id</strong> : {{ $sepulsaDb->product_id }} <br>
				<strong>Product Name</strong> : {{ $sepulsaDb->product_description }} <br>
				<strong>Transaction ID Sepulsa</strong> :  {{ $sepulsaDb->transaction_id }} <br>
				<strong>Order Id</strong> : {{ $sepulsaDb->invoice_id }}
			</td>
		</tr>
		<tr>
			<td>Location</td>
			<td>{{ $sepulsaDb->location }}</td>
		</tr>
		<tr>
			<td>Status</td>
			<td>
				<strong>Status</strong> : {{ $sepulsaDb->status }} <br>
				<strong>RC</strong> : {{ $sepulsaDb->rc }}
			</td>
		</tr>
		<tr>
			<td>Product JSON</td>
			<td>
				<?php 
					$parsed = json_decode($sepulsaDb->product_data);
					if (empty($parsed)) {
						$parsed = [];
					}
				?>
				@foreach ($parsed as $key => $element)
					{{ $key }} : {{ $element }} <br>
				@endforeach
			</td>
		</tr>
	</table>
@endif