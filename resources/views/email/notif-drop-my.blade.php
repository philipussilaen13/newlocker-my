<!DOCTYPE html>
<html>
<head>
    <title>[{{ $data['prefix']  }}] PopBox Customer Drop #{{ $data['invoice_id']  }}</title>
</head>
<body>
<p>
    Hi {{ $data['merchantName'] }},
</p>
<p>
    Your customer has dropped their laundry​ ​for order number <strong>{{ $data['invoice_id']  }}</strong>​, you can find the details of the parcel below:
</p>
<h3>Locker : {{ $data['locker'] }}</h3>
<p>
    Customer Name : {{  $data['cust_name'] }} <br>
    Customer Phone : {{ $data['cust_phone'] }} <br>
    Customer Email : {{ $data['cust_email'] }} <br>
    Drop Time : {{ date('D, j F Y H:i', strtotime($data['dropTime'])) }}
</p>
@if (!empty($detail))
    <p>
        @foreach ($detail as $key => $element)
            {{ $key }} : {{ $element }} <br>
        @endforeach
    </p>
@endif

<p>
    Step by step to take the laundry:
</p>
<ol>
    <li>Please go to  PopBox Locker</li>
    <li>Open "PARCEL DROP-OFF" menu</li>
    <li>Enter Username and Password</li>
    <li>Click button "TAKE" and take the laundry</li>
</ol>
<p>
    Track job status via <a href="https://www.popbox.asia/">www.popbox.asia</a> <br>
    For any support needs feel free to contact us at <a href="mailto:info@popbox.asia" target="_top">info@popbox.asia</a> or <a href="tel:+603 5639 8797">+603 5639 8797</a>
</p>
<p>Thank You</p>
</body>
</html>