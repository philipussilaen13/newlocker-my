<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<?php
    $sepulsaDb = (object)$sepulsaDb; 
?>
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <title></title>


    <style type="text/css" id="media-query">
        body {
            margin: 0;
            padding: 0;
        }

        table, tr, td {
            vertical-align: top;
            border-collapse: collapse;
        }

        .ie-browser table, .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        [owa] .img-container div, [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
            width: 520px !important;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
            width: 172px !important;
        }

        .ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
            width: 344px !important;
        }

        .ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
            width: 260px !important;
        }

        .ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
            width: 173px !important;
        }

        .ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
            width: 130px !important;
        }

        .ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
            width: 104px !important;
        }

        .ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
            width: 86px !important;
        }

        .ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
            width: 74px !important;
        }

        .ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
            width: 65px !important;
        }

        .ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
            width: 57px !important;
        }

        .ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
            width: 52px !important;
        }

        .ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
            width: 47px !important;
        }

        .ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
            width: 43px !important;
        }

        @media only screen and (min-width: 540px) {
            .block-grid {
                width: 520px !important;
            }

            .block-grid .col {
                display: table-cell;
                Float: none !important;
                vertical-align: top;
            }

            .block-grid .col.num12 {
                width: 520px !important;
            }

            .block-grid.mixed-two-up .col.num4 {
                width: 172px !important;
            }

            .block-grid.mixed-two-up .col.num8 {
                width: 344px !important;
            }

            .block-grid.two-up .col {
                width: 260px !important;
            }

            .block-grid.three-up .col {
                width: 173px !important;
            }

            .block-grid.four-up .col {
                width: 130px !important;
            }

            .block-grid.five-up .col {
                width: 104px !important;
            }

            .block-grid.six-up .col {
                width: 86px !important;
            }

            .block-grid.seven-up .col {
                width: 74px !important;
            }

            .block-grid.eight-up .col {
                width: 65px !important;
            }

            .block-grid.nine-up .col {
                width: 57px !important;
            }

            .block-grid.ten-up .col {
                width: 52px !important;
            }

            .block-grid.eleven-up .col {
                width: 47px !important;
            }

            .block-grid.twelve-up .col {
                width: 43px !important;
            }
        }

        @media (max-width: 540px) {
            .block-grid, .col {
                min-width: 320px !important;
                max-width: 100% !important;
            }

            .block-grid {
                width: calc(100% - 40px) !important;
            }

            .col {
                width: 100% !important;
            }

            .col > div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
            }
        }

    </style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">

<!--[if IE]>
<div class="ie-browser"><![endif]-->
<!--[if mso]>
<div class="mso-container"><![endif]-->
<div class="nl-container" style="min-width: 320px;Margin: 0 auto;background-color: #FFFFFF">
    <!--[if (mso)|(IE)]>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" style="background-color: #FFFFFF;"><![endif]-->

    <div style="background-color:transparent;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 520px;width: 520px;width: calc(21000% - 112880px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
             class="block-grid mixed-two-up">
            <div style="border-collapse: collapse;display: table;width: 100%;">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="background-color:transparent;" align="center">
                            <table cellpadding="0" cellspacing="0" border="0" style="width: 520px;">
                                <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                <!--[if (mso)|(IE)]>
                <td align="center" width="173"
                    style=" width:173px; padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                    valign="top"><![endif]-->
                <div class="col num4"
                     style="Float: left;max-width: 320px;min-width: 172px;width: 173px;width: calc(77132px - 14800%);background-color: transparent;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:10px; padding-bottom:0px; padding-right: 10px; padding-left: 10px;">
                            <!--<![endif]-->


                            <div align="center" class="img-container center"
                                 style="padding-right: 0px;  padding-left: 0px;">
                                <!--[if mso]>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="padding-right: 0px; padding-left: 0px;" align="center"><![endif]-->
                                <a href="https://www.popbox.asia/" target="_blank">
                                    <img class="center" align="center" border="0" src="https://www.popbox.asia/img/logo/logo.png" alt="Image"
                                         title="Image"
                                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 120px"
                                         width="120">
                                </a>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>


                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td>
                <td align="center" width="347"
                    style=" width:347px; padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:15px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                    valign="top"><![endif]-->
                <div class="col num8"
                     style="Float: left;min-width: 320px;max-width: 344px;width: 347px;width: calc(3400% - 18016px);background-color: transparent;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                            <!--<![endif]-->


                            <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px;">
                            <![endif]-->
                            <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px;">
                                <div><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: right"><span
                                                style="font-size: 18px; line-height: 21px;">﻿<span
                                                    style="line-height: 21px; font-size: 18px;">Nomor Order:</span></span><br>
                                    </p></div>
                            </div>
                            <!--[if mso]></td></tr></table><![endif]-->


                            <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 10px;">
                            <![endif]-->
                            <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 10px;">
                                <div><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: right"><span
                                                style="color: rgb(51, 102, 255); font-size: 20px; line-height: 24px;"><strong><span
                                                        style="line-height: 24px; font-size: 20px;">{{ $sepulsaDb->invoice_id }}</span></strong></span>
                                    </p></div>
                            </div>
                            <!--[if mso]></td></tr></table><![endif]-->


                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <div style="background-color:transparent;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 520px;width: 520px;width: calc(21000% - 112880px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
             class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="background-color:transparent;" align="center">
                            <table cellpadding="0" cellspacing="0" border="0" style="width: 520px;">
                                <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                <!--[if (mso)|(IE)]>
                <td align="center" width="520"
                    style=" width:520px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                    valign="top"><![endif]-->
                <div class="col num12"
                     style="min-width: 320px;max-width: 520px;width: 520px;width: calc(20000% - 103480px);background-color: transparent;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                            <!--<![endif]-->


                            <div style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
                                <!--[if (mso)]>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="padding-right: 10px;padding-left: 10px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                   border="0">
                                                <tr>
                                                    <td><![endif]-->
                                <div align="center">
                                    <div style="border-top: 1px solid #BBBBBB; width:100%; line-height:1px; height:1px; font-size:1px;">
                                        &nbsp;
                                    </div>
                                </div>
                                <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                            </div>


                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <div style="background-color:transparent;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 520px;width: 520px;width: calc(21000% - 112880px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #DFEAF0;"
             class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="background-color:transparent;" align="center">
                            <table cellpadding="0" cellspacing="0" border="0" style="width: 520px;">
                                <tr class="layout-full-width" style="background-color:#DFEAF0;"><![endif]-->

                <!--[if (mso)|(IE)]>
                <td align="center" width="520"
                    style=" width:520px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                    valign="top"><![endif]-->
                <div class="col num12"
                     style="min-width: 320px;max-width: 520px;width: 520px;width: calc(20000% - 103480px);background-color: #DFEAF0;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                            <!--<![endif]-->


                            <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                            <![endif]-->
                            <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                <div><p style="margin: 0;font-size: 12px;line-height: 14px"><span
                                                style="font-size: 16px; line-height: 19px;"><strong>Rincian Transaksi</strong></span>
                                    </p></div>
                            </div>
                            <!--[if mso]></td></tr></table><![endif]-->


                            <div style="font-size: 16px;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center;">
                                <table style="font-size:14px;color: #555555;">
                                    <tbody>
                                    <tr>
                                        <td align="left"
                                            style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Waktu
                                        </td>
                                        <td align="left">:</td>
                                        <td align="left"
                                            style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">{{ date('j F Y', strtotime($sepulsaDb->created_at)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Produk
                                        </td>
                                        <td align="left">:</td>
                                        <td align="left"
                                            style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">Tagihan BPJS Kesehatan
                                        </td>
                                    </tr>
                                    <?php 
                                        $data = $sepulsaDb->response_data;
                                    ?>
                                    @if (!empty($data))
                                        <?php 
                                            $data = json_decode($data); 
                                            $data = $data->data;
                                        ?>
                                        <tr>
                                            <td align="left"
                                                style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Nama
                                                Pelanggan
                                            </td>
                                            <td align="left">:</td>
                                            <td align="left"
                                                style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">
                                                {{ $data->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Info
                                                Pelanggan
                                            </td>
                                            <td align="left">:</td>
                                            <td align="left"
                                                style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">{{ $data->no_va }} / {{ $data->nama_cabang }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;"> Reference
                                            </td>
                                            <td align="left">:</td>
                                            <td align="left"
                                                style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">{{ $data->sw_reff }}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>


                            <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                <!--[if (mso)]>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                            <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                   border="0">
                                                <tr>
                                                    <td><![endif]-->
                                <div align="center">
                                    <div style="border-top: 1px solid #BBBBBB; width:100%; line-height:1px; height:1px; font-size:1px;">
                                        &nbsp;
                                    </div>
                                </div>
                                <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                            </div>


                            <div style="font-size: 16px;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center;">
                                <table style="font-size:14px;color: #555555;">
                                    <tbody>
                                    <tr>
                                        <td align="left"
                                            style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">
                                            Subtotal
                                        </td>
                                        <td align="left">:</td>
                                        <td align="left"
                                            style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">
                                            <?php $subtotal = $sepulsaDb->product_amount; ?>
                                            Rp{{ number_format($subtotal) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Denda
                                        </td>
                                        <td align="left">:</td>
                                        <td align="left"
                                            style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">Rp0
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Biaya
                                            Admin
                                        </td>
                                        <td align="left">:</td>
                                        <td align="left"
                                            style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">
                                            Rp{{ number_format(2500) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="padding: 5px; width: 30%; line-height: 21px; font-size: 16px;">Total
                                            Bayar
                                        </td>
                                        <td align="left">:</td>
                                        <td align="left"
                                            style="padding: 5px; width: 70%; line-height: 21px; font-size: 16px;">
                                            <?php
                                                $total = $subtotal + 2500;
                                            ?>
                                            Rp{{ number_format($total) }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <div style="background-color:transparent;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 520px;width: 520px;width: calc(21000% - 112880px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
             class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="background-color:transparent;" align="center">
                            <table cellpadding="0" cellspacing="0" border="0" style="width: 520px;">
                                <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                <!--[if (mso)|(IE)]>
                <td align="center" width="520"
                    style=" width:520px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                    valign="top"><![endif]-->
                <div class="col num12"
                     style="min-width: 320px;max-width: 520px;width: 520px;width: calc(20000% - 103480px);background-color: transparent;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!-->
                        <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                            <!--<![endif]-->


                            <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                            <![endif]-->
                            <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                <div><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: justify"><span
                                                style="font-size: 14px; line-height: 16px;">Rincian tagihan dapat diakses di www.bpjs-kesehatan.go.id</span>
                                    </p></div>
                            </div>
                            <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                <div><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: justify"><span
                                                style="font-size: 14px; line-height: 16px;">Bukti ini adalah bukti pembayaran yang sah. Jika ada kendala atau pertanyaan, silahkan menghubungi layanan PopBox di 021-29022537.</span>
                                    </p></div>
                            </div>
                            <!--[if mso]></td></tr></table><![endif]-->


                            <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                <!--[if (mso)]>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                            <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                   border="0">
                                                <tr>
                                                    <td><![endif]-->
                                <div align="center">
                                    <div style="border-top: 1px solid #BBBBBB; width:100%; line-height:1px; height:1px; font-size:1px;">
                                        &nbsp;
                                    </div>
                                </div>
                                <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                            </div>


                            <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                            <![endif]-->
                            <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                <div><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">2017&nbsp;©
                                        PopBox Asia</p></div>
                            </div>
                            <!--[if mso]></td></tr></table><![endif]-->


                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
</div>
<!--[if (mso)|(IE)]></div><![endif]-->


</body>
</html>