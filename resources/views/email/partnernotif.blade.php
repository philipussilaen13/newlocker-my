<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hai Admin, </p>
		<p> Ada kiriman dari {{ $name }} yang harus segera diproses  </p>
		<p>
			Name : {{ $name }} <br />
			Service Name : Business Courier <br />
			Barcode : {{ $barcode }}<br />
			Locker Name : {{ $locker_name }}<br />
			Locker Number : {{ $locker_number }} <br/>
			Locker Size :  {{ $locker_size }} <br/>
			Storetime :   {{ $storetime }} <br/>
			Status :   {{ $status }} <br/>
			Validatecode :  {{ $validatecode }} <br/>
			Overduetime : {{ $overduetime }} <br/>
		</p>		
	</body>
</html>