<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <title>PopShop Invoice</title>
</head>
<body style="font-family:arial">	
	<table cellspacing="0" cellpadding="0" width="100%">
		<tbody>					
			<tr>
				<td>&nbsp;</td>							
					<div style="float: left">
								<img src="{{URL::asset('/img/email/logo-popbox.png')}}"/></div>
								<a href="www.popbox.asia" style="text-decoration: none;font-size: 30px;color: #000;">
									<div style="float: right;margin: 23px;">
									www.popbox.asia
									</div>
								</a>
							<td>&nbsp;</td>
						</tr>			
						<tr>
							<td>&nbsp;</td>
							<td style="text-align: center;color: #fff;background: #4ac0c1">
								<div style="font-size: 25px;margin: 35px;">
									Hi {{$member_name}}
								</div>
								<div style="line-height: 39px;font-size: 19px;">
									Terima kasih sudah mendaftarkan akun PopBox<br/>
									<!-- Mohon klik tombol dibawah ini untuk mengaktifkan akun anda<br/> -->
								</div>
								<!-- <div style="background: #db3a41;width: 40%;margin: 0 auto;padding: 10px;margin-top: 20px; border-radius: 5px;">Aktifkan akun anda
								</div> -->
								<div style="border: 1px solid #FFF;width: 70%;margin: 0 auto;margin-top: 33px;">
								</div>
							</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<div style="text-align: center;color: #fff;background: #4ac0c1;padding: 16px;font-size: 29px;">
								Ingin belanja langsung diPopSHop?
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-align: center;color: #fff;background: #4ac0c1">
								<a href="{{config('config.domain_production')}}/paybyqr" style="text-decoration: none;">
									<div style="background: #db3a41;width: 40%;margin: 0 auto;padding: 10px;margin-bottom: 29px;border-radius: 5px;color: #FFF">		
										Belanja Sekarang
									</div>
								</a>
							</td>
							<td>&nbsp;</td>
						</tr>	
						<tr>
							<td colspan="3">
								<div style="background: #289393;height: 6px;width: 100%">
								</div>								
							</td>							
						</tr>
						<tr>
							<td colspan="3">
								<div style="background: #4ac0c1;width: 100%;text-align: center;color: #FFF;padding:40px 0px;line-height: 40px;font-size: 20px">
									Apabila ada pertanyaan, kami siap membantu anda<br/>
									Silahkan hubungi kami melalui<br/>
									<span>info@popbox.asia</span> atau <span>021 - 29022537</span>
								</div>								
							</td>							
						</tr>
						<tr style="background: #5d5d5e;">
							<td colspan="3" style="text-align: center;height: 50px">
								<a href="https://www.facebook.com/pboxasia"><img src="{{URL::asset('/img/email/logo-fb1.png')}}" style="margin-right: 10%"></a>
								<a href="https://instagram.com/popbox_asia"><img src="{{URL::asset('/img/email/logo-ins.png')}}" style="margin-right: 10%"></a>
								<a href="https://twitter.com/popbox_asia"><img src="{{URL::asset('/img/email/logo-tw.png')}}"></a>
							</td>
						</tr>
					</tbody> 
		</table>		
	</body>
</html>

