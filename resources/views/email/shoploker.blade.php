<html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--><head>
    <!--[if gte mso 9]><xml>
     <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <title>Template Base</title>
    <!--[if !mso]><!-- -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
	<!--<![endif]-->
    
    <style type="text/css" id="media-query">
      body {
  margin: 0;
  padding: 0; }
  a { color: #FFF !important; text-decoration: none !important; }
  
table {
  border-collapse: collapse;
  table-layout: fixed; }

* {
  line-height: inherit; }

a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important; }

.ie-browser .col, [owa] .block-grid .col {
  display: table-cell;
  float: none !important;
  vertical-align: top; }

.ie-browser .corner__x {
  display: none; }

.mso-container .corner__x {
  font-size: 0; }

.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
  width: 500px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%; }

.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
  width: 164px !important; }

.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
  width: 328px !important; }

.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
  width: 250px !important; }

.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
  width: 166px !important; }

.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
  width: 125px !important; }

.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
  width: 100px !important; }

.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
  width: 83px !important; }

.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
  width: 71px !important; }

.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
  width: 62px !important; }

.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
  width: 55px !important; }

.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
  width: 50px !important; }

.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
  width: 45px !important; }

.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
  width: 41px !important; }

@media only screen and (min-width: 520px) {
  .block-grid {
    width: 500px !important; }
  .block-grid .col {
    display: table-cell;
    Float: none !important;
    vertical-align: top; }
    .block-grid .col.num12 {
      width: 500px !important; }
  .block-grid.mixed-two-up .col.num4 {
    width: 164px !important; }
  .block-grid.mixed-two-up .col.num8 {
    width: 328px !important; }
  .block-grid.two-up .col {
    width: 250px !important; }
  .block-grid.three-up .col {
    width: 166px !important; }
  .block-grid.four-up .col {
    width: 125px !important; }
  .block-grid.five-up .col {
    width: 100px !important; }
  .block-grid.six-up .col {
    width: 83px !important; }
  .block-grid.seven-up .col {
    width: 71px !important; }
  .block-grid.eight-up .col {
    width: 62px !important; }
  .block-grid.nine-up .col {
    width: 55px !important; }
  .block-grid.ten-up .col {
    width: 50px !important; }
  .block-grid.eleven-up .col {
    width: 45px !important; }
  .block-grid.twelve-up .col {
    width: 41px !important; } }

@media (max-width: 520px) {
  .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important; }
  .block-grid {
    width: calc(100% - 40px) !important; }
  .col {
    width: 100% !important; }
    .col > div {
      margin: 0 auto; }
  img.fullwidth {
    max-width: 100% !important; } }
      
    </style>
</head>
<!--[if mso]>
<body class="mso-container" style="background-color:#FFFFFF;">
<![endif]-->
<!--[if !mso]><!-->
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<!--<![endif]-->
  <div class="nl-container" style="min-width: 320px;Margin: 0 auto;background-color: #FFFFFF">

    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid two-up">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="250" style=" width:250px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num6" style="Float: left;max-width: 320px;min-width: 250px;width: 320px;width: calc(35250px - 7000%);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div align="left" style="Margin-right: 0px;Margin-left: 0px;">

  <a href="https://www.popbox.asia"><img class="center fullwidth" align="center" border="0" src="{{asset('img/email/logo-popbox.png')}}" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 50%;max-width: 250px;" width="50%"></a>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
              <!--[if (mso)|(IE)]></td><td align="center"  width="250" style="; width:250px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num6" style="Float: left;max-width: 320px;min-width: 250px;width: 320px;width: calc(35250px - 7000%);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div style="Margin-right: 10px; Margin-left: 10px;">  
	<div style="font-size:12px;line-height:14px;font-family:Roboto, Tahoma, Verdana, Segoe, sans-serif;color:#555555;text-align:left;">
		<p style="margin: 0;font-size: 12px;line-height: 14px;text-align: right">
			<span style="font-size: 22px; line-height: 16px; color: rgb(153, 51, 0);"><br></span>
		</p>
		<p style="margin: 0;font-size: 12px;line-height: 14px;text-align: right">
			<span style="font-size: 22px; line-height: 13px; color: rgb(153, 51, 0);"></span>
			</p>
	</div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <img class="center" align="center" border="0" src="{{asset('img/email/logo-delivery.png')}}" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 100%;max-width: 490px" width="490">
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:#FFFFFF;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td bgcolor=&quot;FFFFFF&quot; style="background-color:#FFFFFF;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                
                  
<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;font-family:Roboto, Tahoma, Verdana, Segoe, sans-serif;color:#555555;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px;text-transform: capitalize;">Hi {{$member_name}}</p></div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>
                  
                              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;font-family:Roboto, Tahoma, Verdana, Segoe, sans-serif;color:#555555;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px">
		Terima kasih&nbsp;!
 </p></div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>

<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;font-family:Roboto, Tahoma, Verdana, Segoe, sans-serif;color:#555555;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px">Pemesanan dan﻿ Pembayaran untuk order  <span style="font-weight: bold;">{{$invoice_id}}</span> telah Kami terima.</p>
	<p>
		Rincian pesanan sebagai berikut :
	</p>
	</div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>

                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
  @if (count($detail)>1)
       <div style="Margin-right: 10px; Margin-left: 10px;">        
      @foreach ($detail as $key => $value)
          <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: -webkit-center;margin: 0 auto;">
       
            <table style="font-size:12px;color: #555555;">
              <tbody>
                <tr><td style="width: 150px">SKU</td><td>:</td><td style="width: 200px">{{$value["sku"]}}</td></tr>
                <tr><td>Nama Produk</td><td>:</td><td>{{$value["product_name"]}}</td></tr>
                <tr><td>Jumlah Produk</td><td>:</td><td>{{$value["quantity"]}}</td></tr>
                <tr><td>Harga Satuan</td><td>:</td><td>Rp. {{$value["price"]}}</td></tr>
                <tr><td>Discount</td><td>:</td><td>Rp. {{$value["discount"]}}</td></tr>                
              </tbody>
            </table>
          <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
          </div>
        <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
      @endforeach
        <br/><br/>
          <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: -webkit-center;margin: 0 auto;">       
            <table style="font-size:12px;color: #555555;font-weight: bold">
              <tbody>
                <tr><td style="width: 150px">Jumlah Bayar</td><td>:</td><td style="width: 200px">Rp. {{$pay}}</td></tr>
                <tr><td>Status Bayar</td><td>:</td><td>Lunas</td></tr>
                <tr><td>Nomor Kartu</td><td>:</td><td>{{$no_card}}</td></tr>
                <tr><td>Waktu Bayar</td><td>:</td><td>{{$payment_date}}</td></tr>
                <tr><td>Metode Pembayaran</td><td>:</td><td>Prepaid {{$payment_type}}</td></tr>
                <tr><td>Lokasi Pembayaran</td><td>:</td><td>Loker PopBox {{$loker}}</td></tr>
              </tbody>
            </table>
          <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
          </div>
        <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
      
                    
                                    
                                    
                <!--[if !mso]><!--></div>
@else
  <div style="Margin-right: 10px; Margin-left: 10px;">  
  <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: -webkit-center;margin: 0 auto;">
    <table style="font-size:12px;color: #555555;font-weight: bold">
      <tbody>
        <tr><td>SKU</td><td>:</td><td>{{$detail[0]["sku"]}}</td></tr>
        <tr><td>Nama Produk</td><td>:</td><td>{{$detail[0]["product_name"]}}</td></tr>
        <tr><td>Jumlah Produk</td><td>:</td><td>{{$detail[0]["quantity"]}}</td></tr>
        <tr><td>Harga Satuan</td><td>:</td><td>Rp. {{$detail[0]["price"]}}</td></tr>
        <tr><td>Discount</td><td>:</td><td>Rp. {{$detail[0]["discount"]}}</td></tr>
        <tr><td>Jumlah Bayar</td><td>:</td><td>Rp. {{$pay}}</td></tr>
        <tr><td>Status Bayar</td><td>:</td><td>Lunas</td></tr>
        <tr><td>Nomor Kartu</td><td>:</td><td>{{$no_card}}</td></tr>
        <tr><td>Waktu Bayar</td><td>:</td><td>{{$payment_date}}</td></tr>
        <tr><td>Metode Pembayaran</td><td>:</td><td>Prepaid {{$payment_type}}</td></tr>
        <tr><td>Lokasi Pembayaran</td><td>:</td><td>Loker PopBox {{$loker}}</td></tr>
      </tbody>
    </table>
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
@endif 
      
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
      
<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
  <div style="font-size:12px;line-height:14px;font-family:Roboto, Tahoma, Verdana, Segoe, sans-serif;color:#555555;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px">Barang Anda sedang diproses dan akan dikirimkan segera ke Loker PopBox <span style="font-weight: bold">{{$address}}</span> dalam 3 x 24 jam. Terima kasih telah berbelanja dengan PopBox Loker.</p>  
  </div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>

                  
<div style="Margin-right: 10px; Margin-left: 10px;">
  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center"><span style="font-size: 16px; line-height: 21px; color: rgb(128, 0, 0);">Belanja dengan PopBox Cepat, Mudah, dan Nyaman</span></p></div>

  <div style="line-height: 10px; font-size: 1px">&nbsp;</div>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="500" style=" width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num12" style="min-width: 320px;max-width: 500px;width: 320px;width: calc(18000% - 89500px);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div style="Margin-right: 15px;Margin-left: 15px;background: #db2b26;">
  <div style="line-height: 15px; font-size: 1px">&nbsp;</div>
	<div style="font-size:12px;line-height:14px;font-family:Roboto, Tahoma, Verdana, Segoe, sans-serif;color:#555555;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 17px;text-align: center"><span style="color: #FFF;font-size: 14px;line-height: 16px;">Apabila ada pertanyaan, Kami siap membantu Anda</span><br><span style="color: #FFF;font-size: 14px;line-height: 16px;">Silakan hubungi Kami melalui</span><br><span style="color: #FFF;font-size: 14px;line-height: 16px;"><span style="text-decoration: none;color: #FFF!important">info@popbox.asia</span> atau 021 - 29022537.</span></p></div>

  <div style="line-height: 15px; font-size: 1px">&nbsp;</div>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>    <div style="background-color:transparent;">
      <div rel="col-num-container-box" style="Margin: 0 auto;min-width: 320px;max-width: 500px;width: 320px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid three-up">
        <div style="border-collapse: collapse;display: table;width: 100%;background: #5d5d5e;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td  style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 500px;"><tr class="layout-full-width" style="background-color:transparent;"><td class="corner__x">&nbsp;</td><![endif]-->

              <!--[if (mso)|(IE)]><td align="center"  width="167" style=" width:167px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num4" style="Float: left;max-width: 320px;min-width: 166px;width: 320px;width: calc(77166px - 15400%);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <a href="https://www.facebook.com/pboxasia"><img class="center fullwidth" align="center" border="0" src="{{asset('img/email/logo-fb1.png')}}" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 91%;max-width: 166.666666666667px;" width="166.666666666667"></a>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
              <!--[if (mso)|(IE)]></td><td align="center"  width="167" style="; width:167px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num4" style="Float: left;max-width: 320px;min-width: 166px;width: 320px;width: calc(77166px - 15400%);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <a href="https://instagram.com/popbox_asia"><img class="center fullwidth" align="center" border="0" src="{{asset('img/email/logo-ins.png')}}" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 87%;max-width: 166.666666666667px;" width="166.666666666667"></a>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
              <!--[if (mso)|(IE)]></td><td align="center"  width="167" style="; width:167px; border-top: 0px solid transparent; border-left: 0px solid transparent;  border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-right: 0px; padding-left: 0px;" valign="top"><![endif]-->
            <!--[if !mso]><!--><div rel="col-num-container-box" class="col num4" style="Float: left;max-width: 320px;min-width: 166px;width: 320px;width: calc(77166px - 15400%);background-color: transparent;">
               <div style="background-color: transparent; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"><!--<![endif]-->
                  <div style="line-height: 5px; font-size:1px">&nbsp;</div>
                
                  
<div align="center" style="Margin-right: 0px;Margin-left: 0px;">

  <a href="https://twitter.com/popbox_asia"><img class="center fullwidth" align="center" border="0" src="{{asset('img/email/logo-tw.png')}}" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: 0;height: auto;width: 100%;max-width: 166.666666666667px" width="166.666666666667"></a>
</div>
                  
                                  <div style="line-height: 5px; font-size: 1px">&nbsp;</div>
              <!--[if !mso]><!--></div>
            </div><!--<![endif]-->
          <!--[if (mso)|(IE)]></td><td class="corner__x">&nbsp;</td></tr></table></td></tr></table><![endif]-->
        </div>
      </div>
    </div>  </div>


</div></body></html>