<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Dear Admin  {{ $name }}</p>
		<p> Paket Anda telah berhasil dimasukkan ke loker, kami akan melakukan proses pengiriman untuk order :  <br/>
			AWB : {{ $barcode }}<br />
			Locker Name : {{ $locker_name }}<br />
			Locker Number : {{ $locker_number }} <br/>
			Locker Size :  {{ $locker_size }} <br/>
			Storetime :   {{ $storetime }} <br/>
		</p>
		<p>Terima kasih untuk kepercayaan Anda menggunakan layanan PopBox.</p>
		
Salam Hangat, <br>
<br>
PopBox Asia <br>
Grand Slipi Tower Unit 21J <br>
Jl. Letjen S.Parman Kav 22-24 <br>
Jakarta Barat 11480 <br>
Tlp.021-29022537/38 <br>
www.popbox.asia <br>				
	</body>
</html>