<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    protected $table = 'tb_member_access_token';	

    public static function updateToken($member){
    	$id_member = $member[0]->id_member;        
         $result = self::where('id_member', $id_member)
        ->where('is_login', 1)        
        ->first();    
        $token =  md5($id_member.$member[0]->phone.config('config.chiper_text').time());
        if (empty($result)){
        	$memberToken = new Users;
        	$memberToken->id_member = $id_member;
        	$memberToken->token = $token;
        	$memberToken->save();
        }else{
        	$result->token = $token;
        	$result->save();
        }
        return $token;
    }

    public static function insertMemberToken($member, $type_device=""){
        $id_member = $member[0]->id_member;
        $token =  md5($id_member.$member[0]->phone.config('config.chiper_text').time());
        $memberToken = new Users;
        $memberToken->id_member = $id_member;
        $memberToken->type_device = $type_device;
        $memberToken->is_login = 1;
        $memberToken->created_at = date("Y-m-d H:i:s");
        $memberToken->updated_at = date("Y-m-d H:i:s");
        $memberToken->token = $token;
        $memberToken->save();
        return $memberToken;
    }

    public static function updateMemberTokenLastActivity($token, $type_device=""){          
        $result = self::where('token', $token)
            ->where('type_device', $type_device)
            ->where('is_login', 1)
            ->first();                    
        if (isset($result->updated_at)){
            $result->updated_at = date("Y-m-d H:i:s");
            $result->save();    
            return $result;
        }        
        return null;
    }

    public static function removeMemberTokenByToken($token){
        $result = self::where('token', $token)            
            ->where('is_login', 1)
            ->first();
        if ($result){
            $result->is_login = 0;
            $result->updated_at = date("Y-m-d H:i:s"); 
            $result->save();
            return $result;
        }
        return null;
    }

    public static function checExpiredToken($uid_token=""){
        $result = self::where('token', $uid_token)->first();
        $data = 0;    
        if (isset($result)){
            $data = $result->is_login;
        }
        return $data;
    }

    public static function checkTokenUser($phone, $token){    	    	
    	$data =self::join("tb_member","tb_member.id_member",'=',"tb_member_access_token.id_member")    	
        ->where(function($query) use ($phone){
            $query->where("tb_member.phone",'=',$phone);    
            $query->orWhere("tb_member.email",'=',$phone);
        })
    	->where("tb_member_access_token.token",'=',$token)
    	->select("tb_member.*")
    	->first();        
    	return $data;    	
    }   

    public static function saveLog(){   
        $operationmobile = new \DateTime('1 months ago');
        $datemobile = $operationmobile->format('Y-m-d H:i:s');   
        self::where('updated_at', '>', $datemobile)
        ->where('type_device', 'mobile')
        ->update(['is_login' => 0]);

        $operationweb = new \DateTime('3 days ago');
        $dateweb = $operationweb->format('Y-m-d H:i:s');        
        self::where('updated_at', '>', $dateweb)
        ->where('type_device', 'web')
        ->update(['is_login' => 0]);

        self::where('updated_at', '>', $datemobile)
            ->where('type_device', 'anonymous')
            ->update(['is_login' => 0]);
         \Log::info('web : '. $dateweb.', mobile :'. $datemobile);
    }

    public static function getUserByParams( $params = array() ){
        $db = DB::table("tb_member");
        if (count($params)<0){
        }else{
            if ( isset($params["phone"]) ){
                $db->where("phone",'=',$params["phone"]);
            }
        }
        $data = $db->first();
        return $data;
    }

    public static function getTransactionByParams( $invoice_id = "" ){
        $db = DB::table("orders");        
        $db->where("invoice_id",'=',$invoice_id );        
        $data = $db->count();
        return $data;
    }

    public static function getTransfByParams( $params = array() ){
        $db = DB::table("tb_bank_transfer_payment");
        if (isset($params["invoice_id"])){
            $db->where("invoice_id",'=',$params["invoice_id"]);
        }
        $data = $db->count();
        return $data;
    }

    public static function getUserInvoiceDetail($invoice_id){
         $data = DB::table("tb_member_pickup_parent")
         ->where("id_invoice","=",$invoice_id)
         ->count();
         return $data;        
    }

    public static function getUserInvoiceParent($invoice_id){
        $data = DB::select("SELECT a.id_invoice, 
                case
                    when c.receiver_name is not null then 'COMPLETED'
                    when b.status like '%in_store%' then 'IN_STORE'
                    when b.status like '%taken%' then 'COURIER_TAKEN'
                    when b.status is null then 'NEED_STORE'
                end as status,
               case
                    when c.receiver_name is not null then CONCAT('IN_STORE : ', b.storetime , ', COURIER_TAKEN : ',b.taketime, ', RECEIVED_DATE : ', c.updated_at , ', RECEIVED_BY : ',c.receiver_name)
                    when b.status like '%in_store%' then CONCAT('IN_STORE : ', b.storetime)
                    when b.status like '%taken%' then CONCAT('IN_STORE : ', b.storetime , ', COURIER_TAKEN : ',b.taketime)
                    when b.status is null then 'NEED_STORE'
                End as status_history
            FROM tb_member_pickup_parent a 
            left outer join `locker_activities_pickup` b on a.id_invoice=b.tracking_no
            left outer join return_statuses c ON a.id_invoice=c.number
            where a.id_invoice=?", [$invoice_id]);
        return $data;        
    }

    public static function getUserPickupHistory($phone, $offset){
         $data = DB::select("SELECT a.id_parent, aa.id_invoice,
             case
                when c.receiver_name is not null then 'COMPLETED'
                when b.status like '%in_store%' then 'IN_STORE'
                when b.status like '%taken%' then 'COURIER_TAKEN'
                when b.status is null then 'NEED_STORE'
            end as status,
            case
                when c.receiver_name is not null then CONCAT('IN_STORE : ', b.storetime , ', COURIER_TAKEN : ',b.taketime, ', RECEIVED_DATE : ', c.updated_at , ', RECEIVED_BY : ',c.receiver_name)
                when b.status like '%in_store%' then CONCAT('IN_STORE : ', b.storetime)
                when b.status like '%taken%' then CONCAT('IN_STORE : ', b.storetime , ', COURIER_TAKEN : ',b.taketime)
                when b.status is null then 'NEED_STORE'
            End as status_history
            FROM tb_member_pickup a 
            inner join tb_member_pickup_parent aa on a.id_parent=aa.id_parent 
            left outer join `locker_activities_pickup` b on aa.id_invoice=b.tracking_no
            left outer join return_statuses c ON aa.id_invoice=c.number
            where a.phone=? group by a.id_parent order by a.pickup_order_date desc limit ".$offset.",10", [$phone]);         
         return $data;        
    }

    public static function getUserPickupHistoryCount($phone){
         $data = DB::select("SELECT count(*) as total from (select a.invoice_id, a.pickup_address, a.pickup_locker_name, a.pickup_locker_size, a.item_description,
            a.recipient_name, a.recipient_phone, a.recipient_address, a.recipient_address_detail, a.recipient_locker_name,
            a.recipient_locker_size, a.recipient_email,
            a.amount as total_amount,
            a.pickup_order_date as order_date,
                a.display
            from tb_member_pickup a
            left outer join return_statuses c
            on a.invoice_id=c.number
            left outer join locker_activities_pickup b
            on a.invoice_id=b.tracking_no where a.phone=?
            order by a.pickup_order_date) vtable",[$phone]);         
         $data = $data[0]->total;
         return $data;        
    }

        
		
}
