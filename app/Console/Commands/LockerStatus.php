<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LockerStatus extends Command
{

    var $curl;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lockerstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create schedule task for set status locker to 0';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle(){
        $rangeTime = time()-300; //where no update for 5 minutes
        $query = "SELECT m.* FROM tb_newlocker_machinestat m, tb_newlocker_box b WHERE m.locker_id = b.id AND b.activationFlag = 1 AND m.locker_name not LIKE '%TEST%' AND unix_timestamp(m.update_time) < ".$rangeTime;
        $offlines = DB::select($query);
        //dd($offlines);
        echo "=== CHECK OFFLINE on ".date("Y-m-d H:i:s").":\n";

        if (!empty($offlines)) {
            // update status locker menjadi 0
            foreach ($offlines as $off => $value) {
                $locker_id = $value->locker_id;
                $locker_name = $value->locker_name;
                $core_gui = strpos($value->gui_version, '<[ALL]>');
                $last_update = $value->update_time;
                if ($core_gui == false){
	                DB::table('tb_newlocker_machinestat')
	                    ->where('locker_id', $locker_id)
	                        ->update(array('conn_status' => 0 ));
	                echo "- ".$locker_name." [By GUI Version] \n";
                } else {
                	if (date("Y-m-d", strtotime($last_update)) != date("Y-m-d")) {
                		DB::table('tb_newlocker_machinestat')
                			->where('locker_id', $locker_id)
                			->update(array('conn_status' => 0 ));
                		echo "- ".$locker_name." [By TimeStamp] \n";
                	}
                }
            }
        }
        echo "=== ".count($offlines)." OFFLINE LOCKERS WAS FOUND!\n";
    }
}
