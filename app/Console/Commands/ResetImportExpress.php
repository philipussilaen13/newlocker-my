<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetImportExpress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resetimportexpress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Console to reset express with "IMPORTED" status if more than 1 week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        echo "\n===BEGIN CHECK AT ".date('Y-m-d H:i:s')."\n";
        $tolerance = (time() * 1000) - 604800000;
        $importedExpress = DB::SELECT("SELECT * FROM tb_newlocker_express WHERE deleteFlag <> 1 AND status = 'IMPORTED' AND importTime < ".$tolerance);
        if (count($importedExpress) != 0) {
            foreach ($importedExpress as $import){
                DB::table('tb_newlocker_express')->where('id', $import->id)->update(['deleteFlag' => 1]);
                $awb = $import->expressNumber;
                if (empty($awb)) $awb = $import->customerStoreNumber;
                echo "-> [".$awb."] is reset to deleteFlag 1\n";
            }
        }
        echo "\n===END (".count($importedExpress). ") RECORDS FOUND & RESET\n";
    }

}
