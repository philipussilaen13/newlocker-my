<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
//        Commands\Inspire::class,
        Commands\LockerStatus::class,
        Commands\DbaseBackup::class,
        Commands\SMSReminder::class,
    	Commands\ApacheBackup::class,
        Commands\ForceResync::class,
        Commands\ResetImportExpress::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule){
       // $schedule->command('lockerstatus')->cron('*/5 * * * *')->sendOutputTo('/var/www/pr0x.popbox.asia/storage/app/debugapp/lockerstatus.txt');
//        $schedule->command('dbasebackup')->dailyAt('05:00');
       $schedule->command('smsreminder')->everyMinute()->sendOutputTo('/var/www/pr0x-my.popbox.asia/storage/app/debugapp/smsreminder.txt');
       $schedule->command('resetimportexpress')->dailyAt('00:00')->sendOutputTo('/var/www/pr0x-my.popbox.asia/storage/app/debugapp/resetimportexpress.txt');
       // $schedule->command('forceresync')->cron('*/5 * * * *')->sendOutputTo('/var/www/pr0x.popbox.asia/storage/app/debugapp/forceresync.txt');
    }
}
